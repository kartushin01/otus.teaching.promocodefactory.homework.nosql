﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.Settings
{
    public class MongoDatabaseSettings : IMongoDatabaseSettings
    {
        public string RoleCollectionName { get; set; }
        public string EmployeeCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
    
    public interface IMongoDatabaseSettings
    {
        public string RoleCollectionName { get; set; }
        public string EmployeeCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    } 
 }
