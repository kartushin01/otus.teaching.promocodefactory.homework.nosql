﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Repositories;
using Otus.Teaching.Pcf.Administration.DataAccess.Settings;
using MongoDatabaseSettings = Otus.Teaching.Pcf.Administration.DataAccess.Settings.MongoDatabaseSettings;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoInitializer : IDbInitializer
    {

        private IMongoCollection<Role> _roles;
        private IMongoCollection<Employee> _employe;
        private readonly IMongoDatabaseSettings _settings;
        private IMongoDatabase _database;
        private IMongoClient _client;
        public MongoInitializer(IMongoDatabaseSettings settings)
        {
            _settings = settings;


            _client = new MongoClient(_settings.ConnectionString);

            _database = _client.GetDatabase(_settings.DatabaseName);

         
        }

        public void InitializeDb()
        {
            var empExist = CollectionExists(_database, _settings.EmployeeCollectionName);
            var rolExist = CollectionExists(_database, _settings.RoleCollectionName);
            if (empExist)
            {
                _database.DropCollection(_settings.EmployeeCollectionName);
            }

            if (rolExist)
            {
                _database.DropCollection(_settings.RoleCollectionName);
            }
            

            _employe = _database.GetCollection<Employee>(
                    _settings.EmployeeCollectionName);
            _roles = _database.GetCollection<Role>(
                _settings.RoleCollectionName);

            _employe.InsertMany(FakeDataFactory.Employees);
            _roles.InsertMany(FakeDataFactory.Roles);
        }

        public bool CollectionExists(IMongoDatabase database, string collectionName)
        {
            var filter = new BsonDocument("name", collectionName);
            var options = new ListCollectionNamesOptions { Filter = filter };

            return database.ListCollectionNames(options).Any();
        }
    }
}
