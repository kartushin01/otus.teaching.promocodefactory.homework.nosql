﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Settings;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class EmployeeRepository
    {
        private readonly IMongoCollection<Employee> _employees;

        public EmployeeRepository(IMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var db = client.GetDatabase(settings.DatabaseName);

            _employees = db.GetCollection<Employee>(settings.EmployeeCollectionName);
            
        }

        public async Task<List<Employee>> GetAllAsync()
        {
            var res = await _employees.FindAsync(employee => true);

            return await res.ToListAsync();
        }

        public async Task<Employee> GetByIdAsync(string id)
        {
           var res = await _employees.FindAsync<Employee>(employee => employee.Id == Guid.Parse(id));

           return await res.FirstOrDefaultAsync();
        }

        public Employee Create(Employee employee)
        {
            _employees.InsertOne(employee);
            return employee;
        }

        public async Task UpdateAsync(string id, Employee employeeIn) =>
          await  _employees.ReplaceOneAsync(employee => employee.Id == Guid.Parse(id), employeeIn);

        public async Task RemoveAsync(Employee employeeIn)
        {
            await _employees.DeleteOneAsync(employee => employee.Id == employeeIn.Id);
        }

        public void Remove(string id) =>
            _employees.DeleteOne(employee => employee.Id ==  Guid.Parse(id));


    }
}
