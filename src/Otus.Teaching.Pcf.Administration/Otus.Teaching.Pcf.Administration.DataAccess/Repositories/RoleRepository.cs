﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Settings;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class RoleRepository
    {
        private readonly IMongoCollection<Role> _roles;
        public RoleRepository(IMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _roles = database.GetCollection<Role>(settings.RoleCollectionName);
        }

        public async Task<List<Role>> GetAllAsync()
        {
            return await _roles.Find(role => true).ToListAsync();
        }

    }

}

