﻿using System;
using MongoDB.Bson;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
  
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; }
    }
}