﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Settings;


namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoInitializer : IDbInitializer
    {

        private IMongoCollection<Customer> _customers;
        private IMongoCollection<Preference> _prefetences;
        private IMongoCollection<PromoCode> _promocodes;

        private readonly IMongoDatabaseSettings _settings;
        private IMongoDatabase _database;
        private IMongoClient _client;
        public MongoInitializer(IMongoDatabaseSettings settings)
        {
            _settings = settings;

            _client = new MongoClient(_settings.ConnectionString);

            _database = _client.GetDatabase(_settings.DatabaseName);

         
        }

        public void InitializeDb()
        {
            var customerExist = CollectionExists(_database, _settings.CustomerCollectionName);
            var pereferncesExist = CollectionExists(_database, _settings.PreferenceCollectionName);
            var promocodeExist = CollectionExists(_database, _settings.PromocodeCollectionName);
            
            if (customerExist)
            {
                _database.DropCollection(_settings.CustomerCollectionName);
            }

            if (pereferncesExist)
            {
                _database.DropCollection(_settings.PreferenceCollectionName);
            }

            if (promocodeExist)
            {
                _database.DropCollection(_settings.PromocodeCollectionName);
            }


            _customers = _database.GetCollection<Customer>(_settings.CustomerCollectionName);
            _prefetences = _database.GetCollection<Preference>(_settings.PreferenceCollectionName);
            _promocodes = _database.GetCollection<PromoCode>(_settings.PromocodeCollectionName);

            _customers.InsertMany(FakeDataFactory.Customers);
            _prefetences.InsertMany(FakeDataFactory.Preferences);
           
        }

        public bool CollectionExists(IMongoDatabase database, string collectionName)
        {
            var filter = new BsonDocument("name", collectionName);
            var options = new ListCollectionNamesOptions { Filter = filter };

            return database.ListCollectionNames(options).Any();
        }
    }
}
