﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Settings
{
    public class MongoDatabaseSettings : IMongoDatabaseSettings
    {
        public string CustomerCollectionName { get; set; }
        public string PreferenceCollectionName { get; set; }
        public string PromocodeCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
    
    public interface IMongoDatabaseSettings
    {
        public string CustomerCollectionName { get; set; }
        public string PreferenceCollectionName { get; set; }
        public string PromocodeCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    } 
 }
