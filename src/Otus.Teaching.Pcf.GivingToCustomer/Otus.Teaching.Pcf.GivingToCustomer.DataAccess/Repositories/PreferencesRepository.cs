﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Settings;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class PreferencesRepository
    {
        private readonly IMongoCollection<Preference> _preferences;

        public PreferencesRepository(IMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var db = client.GetDatabase(settings.DatabaseName);

            _preferences = db.GetCollection<Preference>(settings.PreferenceCollectionName);
        }

        public async Task<List<Preference>> GetAllAsync()
        {
            var res = await _preferences.FindAsync(employee => true);

            return await res.ToListAsync();
        }
        public async Task<Preference> GetByIdAsync(string id)
        {
            var res = await _preferences.FindAsync<Preference>(employee => employee.Id == Guid.Parse(id));

            return await res.FirstOrDefaultAsync();
        }

        public Preference Create(Preference book)
        {
            _preferences.InsertOne(book);
            return book;
        }

        public async Task UpdateAsync(string id, Preference employeeIn) =>
            await _preferences.ReplaceOneAsync(employee => employee.Id == Guid.Parse(id), employeeIn);

        public async Task RemoveAsync(Preference employeeIn)
        {
            await _preferences.DeleteOneAsync(employee => employee.Id == employeeIn.Id);
        }

        public void Remove(string id) =>
            _preferences.DeleteOne(employee => employee.Id == Guid.Parse(id));

        public List<Preference> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = _preferences.Find(x => ids.Contains(x.Id));
            return entities.ToList();
        }
    }
}
