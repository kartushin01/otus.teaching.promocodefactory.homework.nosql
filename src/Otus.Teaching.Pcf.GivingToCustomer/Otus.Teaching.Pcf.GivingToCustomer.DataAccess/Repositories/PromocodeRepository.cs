﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Settings;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class PromocodeRepository
    {
        private readonly IMongoCollection<PromoCode> _promoCodes;

        public PromocodeRepository(IMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var db = client.GetDatabase(settings.DatabaseName);

            _promoCodes = db.GetCollection<PromoCode>(settings.PromocodeCollectionName);

        }

        public async Task<List<PromoCode>> GetAllAsync()
        {
            var res = await _promoCodes.FindAsync(promoCode => true);

            return await res.ToListAsync();
        }

        public async Task<PromoCode> GetByIdAsync(string id)
        {
            var res = await _promoCodes.FindAsync<PromoCode>(promoCode => promoCode.Id == Guid.Parse(id));

            return await res.FirstOrDefaultAsync();
        }

        public async Task Create(PromoCode promoCode)
        {
            await _promoCodes.InsertOneAsync(promoCode);
        }

        public async Task UpdateAsync(string id, PromoCode promoCodeIn) =>
            await _promoCodes.ReplaceOneAsync(promoCode => promoCode.Id == Guid.Parse(id), promoCodeIn);

        public async Task RemoveAsync(PromoCode promoCodeIn)
        {
            await _promoCodes.DeleteOneAsync(promoCode => promoCode.Id == promoCodeIn.Id);
        }

        public void Remove(string id) =>
            _promoCodes.DeleteOne(promoCode => promoCode.Id == Guid.Parse(id));

    }
}
