﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Settings;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class CustomerRepository
    {
        private readonly IMongoCollection<Customer> _customers;

        public CustomerRepository(IMongoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var db = client.GetDatabase(settings.DatabaseName);

            _customers = db.GetCollection<Customer>(settings.CustomerCollectionName);

        }
        public async Task<List<Customer>> GetAllAsync()
        {
            var res = await _customers.FindAsync(employee => true);

            return await res.ToListAsync();
        }

        public async Task<Customer> GetByIdAsync(string id)
        {
            var res = await _customers.FindAsync<Customer>(customer => customer.Id == Guid.Parse(id));

            return await res.FirstOrDefaultAsync();
        }

        public Customer Create(Customer employee)
        {
            _customers.InsertOne(employee);
            return employee;
        }

        public async Task UpdateAsync(string id, Customer employeeIn) =>
            await _customers.ReplaceOneAsync(customer => customer.Id == Guid.Parse(id), employeeIn);

        public async Task RemoveAsync(Customer customerIn)
        {
            await _customers.DeleteOneAsync(customer => customer.Id == customerIn.Id);
        }

        public void Remove(string id) =>
            _customers.DeleteOne(customer => customer.Id == Guid.Parse(id));

        public List<Customer> GetWithPreference(Preference preference)
        {
           return _customers.Find<Customer>(customer => customer.PreferencesList.Contains(preference)).ToList();
        }

        public async Task<List<Customer>> GetWithPreferenceAsync(Preference preference)
        {
            var res=  await _customers.FindAsync<Customer>(customer => customer.PreferencesList.Contains(preference));

            return res.Current.ToList();
        }
    }
}
